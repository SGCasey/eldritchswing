﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class objectSelection : MonoBehaviour
{
    public Canvas ui;
    public GameObject building1;
    public GameObject building2;
    public GameObject building3;

    public bool buildingmode;

    private GameObject currentBuilding;

    public void onclick(int buildingtype)
    {
      GameObject building = null;

      switch (buildingtype)
        {
            case 0:
                building = building1;
                buildingmode = true;
                break;
            case 1:
                building = building2;
                buildingmode = true;
                break;
            case 2:
                building = building3;
                buildingmode = true;
                break;
            default:
                break;
        }
        if (building != null)
        {
            Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            point.z = -1f;
            currentBuilding = GameObject.Instantiate(building, point, Quaternion.identity);
            //        Color color = currentBuilding.GetComponent<UnityEngine.UI.Image>().color;
            //color.a = 0.5f;
            //currentBuilding.GetComponent<UnityEngine.UI.Image>().color = color;
        }
        }
    // Start is called before the first frame update
    void Start()
    { 

    }

    // Update is called once per frame
    void Update()
    {
              // controls spawning of object and despawing of object
        if (buildingmode == true)
        {
            Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            point.z = -1f;
            currentBuilding.transform.position = point;


            if(Input.GetMouseButtonDown(0))
                {
                buildingmode = false;
                Color color = currentBuilding.GetComponent<SpriteRenderer>().color;
                color.a = 1f;
                currentBuilding.GetComponent<SpriteRenderer>().color = color;

                }
            }
    }
}