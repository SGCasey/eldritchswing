﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UiUpdate : MonoBehaviour
{
    public TextMeshProUGUI PopulationCount;
    public TextMeshProUGUI Resource1;
    public TextMeshProUGUI Resource2;
    public TextMeshProUGUI Resource3;
    public ParticleSystem Storm;

    public float time = 120;
    public int population = 0;
    public int populationGoal = 10;

    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePopulation(population, populationGoal);
        UpdateResource3(999, 999);
        time -= Time.deltaTime;

        if(time < 0f && time > -5f){
            Storm.Play();
        }
        else if(time < -5f){
            Storm.Stop();
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void UpdatePopulation(int current, int max) {
        
        if(max - current > 5) { //Are we far below the max/recommended? If so, we'll turn the 'current' text red
            PopulationCount.text = $"<color=\"red\">{current}<color=\"white\"> / {max}";
            return;
        }
        PopulationCount.text = $"{current} / {max}";
    }

     public void UpdateResource1(int current, int max) {

        if(max - current > 5) { //Are we far below the max/recommended? If so, we'll turn the 'current' text red
            Resource1.text = $"<color=\"red\">{current}<color=\"white\"> / {max}";
            return;
        }    

        Resource1.text = $"{current} / {max}";
    }

     public void UpdateResource2(int current, int max) {

       if(max - current > 5) { //Are we far below the max/recommended? If so, we'll turn the 'current' text red
            Resource2.text = $"<color=\"red\">{current}<color=\"white\"> / {max}";
            return;
        }   

        Resource2.text = $"{current} / {max}";
    }

     public void UpdateResource3(int current, int max) {   
        // lets go end the game via UI whoooo
        string minutes = Mathf.Floor(time / 60).ToString("00");
        string seconds = (time % 60).ToString("00");
        string text = string.Format("{0}:{1}", minutes, seconds);

        if(time < 46) { //Are we far below the max/recommended? If so, we'll turn the 'current' text red
            Resource3.text = $"<color=\"red\">{text}";
            return;
        }

        Resource3.text = $"{text}";
    }
    

}
