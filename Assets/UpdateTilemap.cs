﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Tilemaps;

public class UpdateTilemap : MonoBehaviour
{

    private Vector3Int tileTarget;
    private Tilemap bldgMap;
    private Vector3 posMouse;
    private Vector3Int posTile;
    private Tile bldgSelected;

    public Tile[] bldgCatalog;
    
    // Start is called before the first frame update
    void Start()
    {
        bldgMap = GameObject.Find("Buildings").GetComponent<Tilemap>();
        bldgSelected = bldgCatalog[0];

    }
     
     bool IsTargetVisible(Camera c,Vector3 point)
     {
         var planes = GeometryUtility.CalculateFrustumPlanes(c);
         foreach (var plane in planes)
         {
             if (plane.GetDistanceToPoint(point) < 0)
             {
                 //Debug.Log($"AYYY {point} isn't in {c}");
                 return false;
             }
         }
         return true;
     }

     public void OnChangeTile(int building){
         bldgSelected = bldgCatalog[building];
     }

    // Update is called once per frame
    void Update()
    {
        posMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        posMouse.z = 0;

        if (Input.GetKeyDown(KeyCode.Alpha1)) { bldgSelected = bldgCatalog[0]; }
        if (Input.GetKeyDown(KeyCode.Alpha2)) { bldgSelected = bldgCatalog[1]; }
        if (Input.GetKeyDown(KeyCode.Alpha3)) { bldgSelected = bldgCatalog[2]; }
        if (Input.GetKeyDown(KeyCode.Alpha4)) { bldgSelected = bldgCatalog[3]; }
        if (Input.GetKeyDown(KeyCode.Alpha5)) { bldgSelected = bldgCatalog[4]; }

        if (Input.GetMouseButtonDown(0) && IsTargetVisible(Camera.main, posMouse))
        {
            posTile = bldgMap.WorldToCell(posMouse);
            bldgMap.SetTile(posTile, bldgSelected);
        }
    }
}
