﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class ThisDoesTheThingy : MonoBehaviour
{
    public float mSpeed = 10.0f;
    public float mDelta = 6000.0f;
    
    private void Update()
    {
        // Check if on the right edge
        if ( Input.GetKey(KeyCode.D) || Input.mousePosition.x >= (Screen.width - (int)mDelta) )
        {
            // Move the camera
            transform.position += Vector3.right * Time.deltaTime * mSpeed;
        }
        // Check if on the right edge
        if ( Input.GetKey(KeyCode.S) || Input.mousePosition.y <= 0 + mDelta )
        {
            // Move the camera
            transform.position += Vector3.down * Time.deltaTime * mSpeed;
        }

        // Check if on the right edge
        if ( Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - mDelta )
        {
            // Move the camera
            transform.position += Vector3.up * Time.deltaTime * mSpeed;
        }
                // Check if on the right edge
        if ( Input.GetKey(KeyCode.A) || Input.mousePosition.x <= 0 + mDelta )
        {
            // Move the camera
            transform.position += Vector3.left * Time.deltaTime * mSpeed;
        }
        
     
    }
    private void LateUpdate()
    {
    }
}